# efikaGreenPayMagento

## Steps to configure Greenpay Extension
1. Disable cache management from system->cache management in Admin Panel
2. Paste the "app/etc/modules/Efika_GreenPay.xml" file in "path_to_magento\app\etc\modules"
3. Paste the "app/locales/es_CR/Efika_GreenPay.csv" file in "path_to_magento\app\locales\es_CR"
4. Create a folder and name it "local" in path_to_magento\app\code\
5. Paste "app/code/local/Efika" folder in "path_to_magento\app\code\local"
6. Restart Magento.