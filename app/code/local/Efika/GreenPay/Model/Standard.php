<?php

class Efika_GreenPay_Model_Standard extends Mage_Payment_Model_Method_Abstract
{
    protected $_code = 'GreenPay';

    protected $_isInitializeNeeded = true;
    protected $_canUseInternal = false;
    protected $_canUseForMultishipping = false;

    /**
     * Checkout redirect URL getter for onepage checkout (hardcode)
     *
     * @see Mage_Checkout_OnepageController::savePaymentAction()
     * @see Mage_Sales_Model_Quote_Payment::getCheckoutRedirectUrl()
     * @return string
     */
    public function getOrderPlaceRedirectUrl()
    {
        return Mage::getUrl('greenpay/index/init');
    }
}
