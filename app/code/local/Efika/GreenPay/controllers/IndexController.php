<?php
/**
 * Efika_GreenPay_IndexController Controls the payment flow with greenpay.
 *
 * @package GreenPay/Payment
 * @author Efika <andres.segreda@efika.solutions>
 * @author Minor Umana Murillo <mumana@greenpay.me>
 * @since 1.8.0
 * @version $Revision: 3.0.0
 */
class Efika_GreenPay_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * initAction receives the redirect from getOrderPlaceRedirectUrl function model.
     * Also controls the checkout order creation process with GreenPay.
     *
     * @throws Exception When can't create the payment order in GreenPay.
     * @throws Exception When the currency order isn't allowed in GreenPay.
     *
     * @return void
     */
    public function initAction()
    {
        try {
            /** @var Mage_Core_Model_Session $session */
            $session = Mage::getSingleton('core/session');

            // Loads order from orderId
            $orderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
            $order = Mage::getSingleton('sales/order')->loadByIncrementId($orderId);

            // Gets callback URL
            $url = Mage::getStoreConfig('payment/GreenPay/submit_url');
            $payload = $this->createOrderPaymentPayload();

            // Creates checkout order on GreenPay
            $response = $this->postGreenPayOrder($url, $payload);
            Mage::helper('GreenPay')->log('Create order response:'.print_r($response, true), true);

            // Checks if the order currency is supported by GreenPay
            if ($this->isAnAllowedCurrency($order->getOrderCurrencyCode())) {
                if ($sessionOrder = $response->session) {
                    $order->setState(Mage_Sales_Model_Order::STATE_PENDING_PAYMENT)
                    ->setStatus(Mage_Sales_Model_Order::STATE_PENDING_PAYMENT)
                    ->save();
                    // Loads the payment object
                    $payment = $order->getPayment();

                    //Set GreenPay Reply order values to quote_payment.
                    $payment->setAdditionalInformation('GreenPay Session', $response->session);
                    $payment->setAdditionalInformation('GreenPay Token', $response->token);
                    $payment->save();

                    // Redirects to GreenPay Payment Gateway form
                    $this->_redirectUrl($this->getGreenPayRedirectUrl($sessionOrder));
                    return;
                }

                throw new Exception(Mage::helper('GreenPay')->__('Unable to start GreenPay Checkout. Please try again or contact us.'));
            }

            throw new Exception(Mage::helper('GreenPay')->__('Currency dont allowed in GreenPay. Please contact to Admin store.'));
        } catch (Exception $e) {
            $session->addError($e->getMessage());
            Mage::helper('GreenPay')->log($e->getMessage(), true);

            $this->_redirect('checkout/cart');
        }
    }

    /**
     * Redirect function. Controlls the greenpay checkoutForm redirection to magento.
     * Checks if the user cancel the order, if not, call to process the response.
     * @var string $orderId
     * @var Magento_Order_Object $order
     * @var string $base64String : Response in base64 attached to the redirect URL
     * @var object $response : JSON with greenpay checkout response.
     *
     * @return void
     */

    public function callbackAction()
    {
        Mage::helper('GreenPay')->log("CallbackAction URL response string" .$this->getRequest()->getParams()["response"], true);
        $orderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
        $order = Mage::getSingleton('sales/order')->loadByIncrementId($orderId);
        try {
            $base64String = $this->getRequest()->getParams()["response"];
            if ($base64String === "cancel") {
                $order->setState(Mage_Sales_Model_Order::STATE_CANCELED)
                ->setStatus(Mage_Sales_Model_Order::STATE_CANCELED)
                ->save();
                // $order->cancel();
                $history = $order->addStatusHistoryComment('Error: Cancelada por el usuario', true);
                $order->save();
                $this->restoreCart($orderId);

                $this->getOnepage()->getCheckout()->addNotice($this->__('You canceled payment.'));
                $this->_redirect('checkout/cart');
            } else {
                $base64Decoded = base64_decode($base64String);
                $response = json_decode($base64Decoded);
                // Mage::helper('GreenPay')->log("checkoutForm Response :" . "\n".print_r($response, true));
                return $this->greenpayResponse($response);
            }
        } catch (Exception $e) {
            $order->setState(Mage_Sales_Model_Order::STATE_CANCELED)
            ->setStatus(Mage_Sales_Model_Order::STATE_CANCELED)
            ->save();
            $history = $order->addStatusHistoryComment($e->getMessage(), false);
            $order->save();

            $this->restoreCart($orderId);
            $session->addError($this->__('Unable to get GreenPay Response. Please try again'));
            Mage::helper('GreenPay')->log('callbackAction Error:' . "\n" . $e->getMessage(), true);

            $this->_redirect('checkout/cart');
        }
    }

    /**
     * Receives post checkout responses from GreenPay.
     *
     * @return void
     */
    public function webhookAction()
    {
        if ($this->getRequest()->isPost()) {
            $body = $this->getRequest()->getRawBody();
            $data = Zend_Json::decode($body, Zend_Json::TYPE_OBJECT);
            Mage::helper('GreenPay')->log('webhookAction' . "\n" .  print_r($data, true), true);
            $response = array(
                'code'    => 500,
                'message' => 'Error'
            );
            try {
                if(!empty($data)){
                    // Checks if the response comes from GreenPay
                    if ($this->isFromGreenPay($data)) {
                        $response['code'] = 200;
                        $response['message'] = 'Success';
                        $this->addPaymentInfoToOrder($data);
                        print_r($response);
                        exit;
                    }

                    throw new Exception(Mage::helper('GreenPay')->__('Verify response. Response received dont came from GreenPay.'));
                }
                throw new Exception(Mage::helper('GreenPay')->__('Verify response. Response received dont came from GreenPay. Null data'));
            } catch (Exception $e) {
                Mage::helper('GreenPay')->log('webhookAction Error' . "\n" . $e, true);
                print_r($response);
                exit;
            }
        }
        print_r($response);
        exit;
    }

    /**
     * Adds greenpay payment information to order.
     *
     * @param  object $transaction : GreenPay checkout response data.
     *
     * @throws Exception When order has the payment information.
     *
     * @return void
     */
    private function addPaymentInfoToOrder($transaction)
    {
        try {
            $order = Mage::getSingleton('sales/order')->loadByIncrementId($transaction->orderId);
            $order->getPayment()->setAdditionalInformation('responseCode', $transaction->status);
            Mage::helper('GreenPay')->log('addPaymentInfoToOrder:' . "\n" .  print_r($transaction, true), true);
            $paymentInfo = $order->getPayment()->getAdditionalInformation();
            if (!isset($paymentInfo['transactionStatus']) || ($paymentInfo['transactionStatus'] !== 'Authorized' && $paymentInfo['transactionStatus'] !== 'Denied')) {
                if (is_object($transaction->result)) {
                    $order->getPayment()->setTransactionId($transaction->result->retrieval_ref_num);
                    $order->getPayment()->setAdditionalInformation('last4Digits', $transaction->last4);
                    $order->getPayment()->setAdditionalInformation('cardBrand', $transaction->brand);
                    $order->getPayment()->setTransactionAdditionalInfo(
                        Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS,
                        array(
                            "authorization" =>$transaction->authorization,
                            "orderId" =>  $transaction->orderId,
                            "bankReference" => $transaction->result->retrieval_ref_num,
                            "errorMessage" => $transaction->result->reserved_private4,
                            "CardLast4" => $transaction->last4,
                            "cardBrand" => $transaction->brand
                        )
                    );

                    if ($transaction->status === 200) {
                        // Add transaction information
                        $order->getPayment()->setAdditionalInformation('transactionStatus', 'Authorized');
                        $order->getPayment()->setAdditionalInformation('authorizationNumber', $transaction->authorization);
                        $order->getPayment()->registerCaptureNotification($order->getGrandTotal());

                        // Update order status
                        $order->setData('state', Mage_Sales_Model_Order::STATE_COMPLETE);
                        $order->setStatus(Mage_Sales_Model_Order::STATE_COMPLETE);
                        $history = $order->addStatusHistoryComment(sprintf(__('Payment accepted. Authorization number: %s'), $transaction->authorization), true);
                        $order->save();
                    } else {
                        $order->getPayment()->setAdditionalInformation('transactionStatus', 'Denied');
                        $order->getPayment()->setAdditionalInformation('errorCode', $transaction->result->resp_code);
                        $order->getPayment()->setAdditionalInformation('errorMessage', $transaction->result->reserved_private4);
                        if ($this->isFraudError($transaction->result->resp_code)) {
                            $text = __('The user has fraud behavior, please contact to him. Transaction error: %s');
                            $order->setState(Mage_Sales_Model_Order::STATE_CANCELED)
                            ->setStatus('fraud')
                            ->save();
                            $history = $order->addStatusHistoryComment(sprintf($text, $response->result->reserved_private4), false);
                            $order->save();
                        } else {
                            $text = __('Payment rejected. Error: %s');
                            $order->setState(Mage_Sales_Model_Order::STATE_CANCELED)
                            ->setStatus(Mage_Sales_Model_Order::STATE_CANCELED)
                            ->save();
                            $history = $order->addStatusHistoryComment(sprintf($text, $transaction->result->reserved_private4), true);
                            $order->save();
                        }
                    }
                } else {
                    $text = __('Unsuccessfull transaction. Error: %s');
                    $order->setState(Mage_Sales_Model_Order::STATE_CANCELED)
                                    ->setStatus(Mage_Sales_Model_Order::STATE_CANCELED)
                                    ->save();
                    $history = $order->addStatusHistoryComment(sprintf($text, $transaction->errors), true);
                    $order->save();
                }
            }
        } catch (Exception $e) {
            Mage::helper('GreenPay')->log('addPaymentInfoToOrder Error:' . "\n" .  print_r($e, true), true);
        }
    }

    /**
     * Checks if the data received in the hook comes from greenpayResponse
     * @param stdclass $response Response object sent by GreenPay
     * @param boolean $isCheckout true if the response object is a checkout service response
     * @return boolean
     */
    private function isFromGreenPay($response, $isCheckout=true)
    {
        $publicKey = Mage::getStoreConfig('payment/GreenPay/pkey');
        $signature = hex2bin($response->_signature);

        if ($isCheckout) {
            // Buils the object to be validate
            $toValidate = 'status:'.$response->status.',orderId:'.$response->orderId;
        } else {
            // Buils the object to be validate
            $toValidate = 'status:'.$response->status.',orderId:'.$response->requestId;
        }
        // Loads the public key from extension config
        return openssl_verify($toValidate, $signature, openssl_get_publickey($publicKey), OPENSSL_ALGO_SHA256);
    }

    /**
     * greenpayResponse
     *
     * @param  mixed $response
     *
     * @return void
     */
    private function greenpayResponse($response)
    {
        $session = Mage::getSingleton('core/session');
        $orderId = $this->getCheckoutSession()->getLastRealOrderId();
        $order = Mage::getSingleton('sales/order')->loadByIncrementId($orderId);
        $order->getPayment()->setAdditionalInformation('responseCode', $response->status);
        try {
            if (isset($response->result)) {
                if ($response->status === 200) {
                    $message = __('Payment accepted');
                    $session->addSuccess($message);
                    $this->addPaymentInfoToOrder($response);
                    $this->_redirect('checkout/onepage/success');
                    return;
                } else {
                    $text=__('Payment rejected. Error: %s');
                    if ($this->isFraudError($response->result->resp_code)) {
                        $message = sprintf($text, __('Invalid transaction'));
                    } else {
                        $message = sprintf($text, $response->result->reserved_private4);
                        $this->restoreCart($orderId);
                    }

                    $session->addError($message);
                    $this->_redirect('checkout/onepage/failure');
                    return;
                }

                // return $redirect;
            }

            throw new Exception(Mage::helper('GreenPay')->__('Unable to get bank response. Please check your bank account before try again.'));
        } catch (Exception $e) {
            Mage::helper('GreenPay')->log('greenpayResponse Error' . "\n" . $e->getMessage(), true);
            Mage::helper('GreenPay')->log('greenpayResponse json response' . "\n" .  print_r($response, true), true);

            if (isset($response->status) && isset($response->readyState) && $response->status === 500) {
                $order->getPayment()->setAdditionalInformation('transactionStatus', 'Timeout');
                $session->addError($this->__('There was a timeout getting the bank response, please check your bank account and wait for at least 5 minutes before try again.'));
            } else {
                $session->addError($this->__('There was a error in the payment request, please check the card details.'));
                $session->addError($this->print_r($response->errors, true));
                $this->restoreCart($orderId);
            }

            return $this->_redirect('checkout/cart');
        }
    }

    /**
     * restoreCart
     *
     * @param  mixed $orderId
     *
     * @return void
     */
    private function restoreCart($orderId)
    {
        $session = Mage::getSingleton('checkout/session');
        $cart = Mage::getSingleton('checkout/cart');
        $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
        if ($order->getId()) {
            $session->getQuote()->setIsActive(false)->save();
            $session->clear();
            // try
            // {
            // 	$order->setActionFlag(Mage_Sales_Model_Order::ACTION_FLAG_CANCEL, true);
            // 	$order->cancel()->save();
            // }
            // catch (Mage_Core_Exception $e)
            // {
            // 	Mage::logException($e);
            // }
            $items = $order->getItemsCollection();
            foreach ($items as $item) {
                try {
                    $cart->addOrderItem($item);
                } catch (Mage_Core_Exception $e) {
                    $session->addError($this->__($e->getMessage()));
                    Mage::helper('GreenPay')->log("restoreCart : \n".$e);
                    continue;
                }
            }
            $cart->save();
        }
    }

    /**
     * @return Mage_Checkout_Model_Type_Onepage
     */
    public function getOnepage()
    {
        return Mage::getSingleton('checkout/type_onepage');
    }

    public function getCheckoutSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    private function createOrderPaymentPayload()
    {
        $orderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
        Mage::helper('GreenPay')->log('order' . "\n" .  print_r($orderId, true), true);
        $order = Mage::getSingleton('sales/order')->loadByIncrementId($orderId);
        $callback = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);

        $payload = json_encode(array(
            "secret" => Mage::getStoreConfig('payment/GreenPay/secret'),
            "merchantId" => Mage::getStoreConfig('payment/GreenPay/merchant_id'),
            "terminal" => Mage::getStoreConfig('payment/GreenPay/terminal_id'),
            "amount" => (float) number_format((float) $order->getGrandTotal(), 2, '.', ''),
            "currency" => $order->getOrderCurrencyCode(),
            "description" => "Compra orden " . $orderId,
            "orderReference" => $orderId,
            "callback" => $callback.'greenpay/index/callback/response',
            "additional"     => $this->getCustomerAdditionalData()
        ));

        return $payload;
    }

    /**
     * isAnAllowedCurrency
     *
     * @param  mixed $currencyCode
     *
     * @return boolean
     */
    private function isAnAllowedCurrency($currencyCode)
    {
        $allowedCurrencies = array('USD', 'CRC', 'GTQ');

        return in_array($currencyCode, $allowedCurrencies);
    }

    private function isFraudError($code)
    {
        $fraudCodes = array('04', '41', '43');

        return in_array($code, $fraudCodes);
    }

    /**
     * postGreenPayOrder
     *
     * @param  mixed $url
     * @param  mixed $payload
     *
     * @return void
     */
    private function postGreenPayOrder($url, $payload)
    {
        // echo print_r($payload,true);
        $ch = curl_init($url);
        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        //set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //execute the POST request
        $result = curl_exec($ch);
        curl_close($ch);
        $obj = json_decode($result) ;
        Mage::helper('GreenPay')->log(print_r($obj, true), true);
        return $obj;
    }

    /**
     * getProductDetails
     *
     * @return void
     */
    private function getProductDetails()
    {
        // retrieve quote items array
        $orderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
        $order = Mage::getSingleton('sales/order')->loadByIncrementId($orderId);
        $items = $order->getAllItems();
        $products = array();
        foreach ($items as $item) {
            $object['description'] = $item->getName();
            $object['skuId']       = $item->getProductId();
            $object['quantity']    = intval($item->getQtyOrdered());
            $object['price']       = (float) number_format((float) $item->getPrice(), 2, '.', '');
            $object['type']        = $item->product_type;
            array_push($products, $object);
        }
        return $products;
    }

    /**
     * getCustomerAdditionalData
     *
     * @return void
     */
    private function getCustomerAdditionalData()
    {
        $orderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
        $order = Mage::getSingleton('sales/order')->loadByIncrementId($orderId);

        $billingAddress = array(
            'country'  => $order->getBillingAddress()->country_id,
            'province' => $order->getBillingAddress()->region,
            'city'     => $order->getBillingAddress()->city,
            'street1'  => $order->getBillingAddress()->street,
            'zip'      => $order->getBillingAddress()->postcode
        );

        $shippingAddress = $billingAddress;

        if (!is_null($order->getShippingAddress()->getData())) {
            $shippingAddress = array(
                'country'  => $order->getShippingAddress()->country_id,
                'province' => $order->getShippingAddress()->region,
                'city'     => $order->getShippingAddress()->city,
                'street1'  => $order->getShippingAddress()->street,
                'zip'      => $order->getShippingAddress()->postcode
            );
        }

        return array(
            'customer' => array(
                'name' => $order->getBillingAddress()->firstname .' '.$order->getBillingAddress()->lastname,
                'email' => $order->getBillingAddress()->email,
                'shippingAddress' => $shippingAddress,
                'billingAddress' => $billingAddress
            ),
            "products" => $this->getProductDetails()
        );
    }

    /**
     * getGreenPayRedirectUrl
     *
     * @param  mixed $session
     *
     * @return string
     */
    private function getGreenPayRedirectUrl($session)
    {
        // $isTestMode = Mage::helper('cybersource_core')->getIsTestMode();
        return Mage::getStoreConfig('payment/GreenPay/form_url') . "/" . $session;
    }
}
